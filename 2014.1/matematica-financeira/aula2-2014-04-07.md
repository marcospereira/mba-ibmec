# Conceitos para estudar

1. Matemática financeira: Markup
2. HP 12c

## HP 12c

1. `CLX`: Limpa apenas o visor
2. `f REG`:
	1. Limpa o visor
	2. Limpa a pilha de execução
	3. Limpa memórias
	4. Limpa teclas financeiras
3. `f FIN`: Limpa teclas financeiras

#### Calculo de juros Simples x Compostos

1. Ativar juros compostos: `STO` + `EEX`
2. Com modo ativado, sempre calcula juros compostos, mesmo que o período seja menor do que 1
3. Com modo desativado, calcula juros simples se periodo for menor do que 1 e composto se for maior do que 1

#### Calcular juros por tempo

1. Para investimentos:
	1. `STO` + `EEX` (para ativar calculo com juros compostos)
	2. Valor + `CHS` (investimentos contam como saida de dinheiro, ou seja, negativo)  + `PV`
	3. Taxa + `i`
	4. Quantidade de meses + `n`
	5. `FV` (FV = 'Future Value')
	6. Depois basta alterar a quantidade de meses + `n` + `FV`
2. Calculando taxas de juros:
	1. `STO` + `EEX` (para ativar calculo com juros compostos)
	2. Valor + `CHS` (investimentos contam como saida de dinheiro, ou seja, negativo) + `PV`
	3. Valor futuro + `FV`
	4. Quantidade de meses + `n`
	5. `i` para ver a taxa de juros

------------------

Exercício 3.43, página 97:

Quando existe comparação de várias formas de pagamento, para o mesmo produto, tentar trazer todas as opções para o valor atual. Assim fica mais fácil comparar as taxas de juros, tanto da compra quanto do investimento.

------------------

Dicas:

Surpreenda.com.br - para o MasterCard
naotempreco.com.br - para o MasterCard

------------------

Próxima aula:

1. Fazer exercícios 3.34, 3.35, 3.36
