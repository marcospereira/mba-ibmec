# Aula 7

Essa aula foi dada antes da aula 6 porque é uma aula de reposição.

## Sistemas de Amortização

1. **`PRICE`**: Parcela mais baixa no inicio, mas paga mais juros
2. **`SAC`**: Parcela mais alta no inicio, mas paga menos juros. Outra vantagem é que como as primeiras parcelas são mais altas, o risco de inadimplência é mais baixo conforme as parcelas são pagas

