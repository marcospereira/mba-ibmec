# Revisão da Disciplina

Exercícios a serem resolvidos:

1. Cap 3   - 2
3. Cap 4   - 3
4. Cap 5/8 - 2
6. Cap 6   - 1
5. Cap 7   - 2


## Cap 3

### Primeiro problema:

Essa é uma questão de taxa de equivalencia, ou seja, transformar uma taxa em um periodo e transformar para outro período.

> 1. Taxa selic: 11% aa. Valida para 252 dias uteis
> 2. Uma aplicação em 12 de Junho de 2014, até 17/09/2014 renderia quanto?
> 3. Supor 80 dias uteis.

#### Solução

Taxa de um periodo para o outro:

1. 1,11^(80/252)

Ou:

1. PV (CHS) = 100
2. FV = 111
3. n = 252
4. Encontra i

Taxa = 0,04142 ao dia

1. PV (CHS) = 100
2. i = taxa encontrada acima
3. n = 80
4. Encontra FV

FV = 103,3685

Isso quer dizer que a taxa de juros para 80 dias é igual a 3,3685

#### Outra solução

1. PV (CHS) = 100
2. i = 11%
3. n = 80/252
4. Encontra FV = 103,3685

### Segundo problema:

Compra e venda de titulos, com mudanças de taxa de juros entre o momento inicial da compra e de revenda.

> Luciana investiu em uma LTN prazo 4 anos, taxa de 12% aa após 1 ano de investimento, vendeu para Sabrina e nesta epoca a taxa subiu para 14% aa.

Pergunta:

1. Quanto cada uma ganhou em termos anuais?

Como é uma LTN, vamos estipular um valor futuro arbitrário, já que queremos apenas a taxa.

FV = R$ 1000,00

Vamos calcular o valor presente desse titulo:

1. FV = 1000
2. i = 12%
3. n = 4
4. Encontramos o valor pago pelo titulo no momento zero: R$ -635,5180

Vamos encontrar quanto vale no momento da venda. Como o valor futuro do titulo e o valor pago não mudam, precisamos encontrar o valor PV no momento da venda, considerando a nova taxa de juros:

1. FV = 1000
2. n = 3
3. i = 14%
4. Encontramos o PV do momento da compra = R$ -674,9715

Agora que temos o valor inicia de compra e o valor de revenda, vamos calcular a taxa para Luciana:

1. FV = 674,9715
2. PV (CHS) = -635,5180
3. n = 1
4. Encontramos o i = 6,0808%

A taxa de ganhos para Sabrina é a taxa no momento da compra, 14%

#### Alterando o problema:

Supor que **no primeiro ano** o IPCA variou 10% e a LTN pagasse a Taxa Anunciada + IPCA.


## Capitulo 4

> Renata tem uma filha de 4 anos e quer planejar seu estudo na faculdade a partir dos 18 anos. Hoje: Faculdade 36.000/aa, 5 anos, taxa juros 9% aa; inflação 6%; Quanto Renata deveria guardar por ano?

**Ver ultima página do material de Marketing para fluxo de caixa.**


#### Fluxo de caixa:

1. Depositos anuais entre os 4 e 17 (inclusive) anos
2. 5 retiradas, 1 a cada ano da universidade

Primeiro vamos encontrar a taxa de juros real, considerando a inflação:

1. 1,09/1.06 = 1,0283

Ou seja, taxa de juros real = 2,83. Vamos encontrar quanto Renata precisa ter no primeiro ano da universidade.

1. i = 2,83
2. PMT = 36000
3. n = 5
4. PV = 165.672,7833

Agora encontramos o PMT considerando:

1. FV = 165.672,7833
2. i = 2,83
3. n = 14
4. Encontramos PMT = 9.808,43


## Capitulos 5 & 8

1. Investimento inicial: 500.000,00
2. Custo de Oportunidade: 10% aa
3. Parcelas anuais: 60.000,00

Encontrar:

1. Preço Justo?
2. VPL?
3. TIR

Faz ou não faz?

    Preco justo = A/i

É quanto preciso colocar para render as parcelas anuais, na taxa dada. Ou seja, se quero tirar 60.000,00 com uma taxa de 10%, deveria investir 600.000,00

    Preco justo = 60.000/0,10 = 600.000,00

E o VPL:

    VPL = Valor Presente Liquido = Net Present Value = 100.000,00

### E se fossem 20 anos e não para sempre?

Vamos encontrar o preço justo:

1. PMT = 60,000 
2. i = 10%
3. n = 20
4. Encontramos o PV = -510.813,82

E a partir disso:

VPL = 10.813,82

E a Taxa interna de Retorno

1. PV (CHS) = 500.000,00
2. n = 20
3. PMT = 60,000
4. Encontramos o i = 10.3156%


## Capitulo 7

> 1. Financiamento 300.000,00
> 2. 300 meses
> 3. 2% am
> 4. Após pagar 100 parcelas você pagou R$ 50.000,00 adicionais

Questões:

1. Qual a nova prestação mantendo o prazo restante?
2. Qual o novo prazo se mantendo a prestação constante?

Vamos calcular o valor da parcela:

1. PV = 300.000,00
2. n = 300
3. i = 2
4. Encontramos o PMT = -6.015,82

A partir disso encontramos o PV para 200 prestações, considerando esse pagamento:

1. n = 200
2. Encontramos o PV = 295.060,064

Como existe um adiantamento de 50.000, o saldo devedor é 245.060,064.

Agora vamos encontrar qual o valor da nova prestação mantendo o prazo:

1. PV = 245.060,064
2. n = 200
3. i = 2
4. Encontramos o PMT = -4.996,39

Agora vamos encontrar quanto de prazo se mantivermos a prestação de R$ 6.015,82:

1. PV = 245.060,064
2. i = 2
3. PMT (CHS) = 6.015,82
4. Encontramos o n = 86 parcelas restantes