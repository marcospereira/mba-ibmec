# Aula 3

1. 3 pontos da nota
	1. 2 resumos (recomendação, textos 3 e 4)
	2. Autozone
	3. Entregar impressos

## Os Ambientes de Marketing

1. Informação é a matéria prima
2. As teorias são as ferramentas para analisar as informações

Em marketing: teorias de marketing e informações de mercado

## O que é planejamento?

1. "Antecipar um futuro" (como alcançar um objetivo)
2. Dos 3 tempos (passado, presente, futuro) só o passado existe
3. O presente é feito para o ato de planejar
4. "Definição do Objetivo": um ponto marcado do futuro
5. O passado serve de matéria prima -> histórico -> analise -> dados/informações

## Paralelo com WAR

1. O tabuleiro, em planejamento, é o micro ambiente (mercado definido)
2. A mesa é o macro ambiente (onde o mercado está inserido)
3. O macro ambiente afeta todos os jogadores (por exemplo, mudanças na economia, tragédias, etc)

Limitar o tabuleiro?

#### Ambientes de Marketing

1. 3 C's
	1. Cia: Ambiente interno
	2. Concorrentes + Clientes: Ambiente externo
	3. Cliente: Ambiente do consumidor
	4. Concorrentes: Ambiente do concorrente

## As 5 forças de Porter

LIVRO: Estratégia Competitiva, Michael Porter
ARTIGO: Miopia de Marketing

As 5 forças de Potter servem para avaliar um setor da economia (Potter chama setores de industria - industria automobilistica, bancário, etc).

As forças são:

1. Rivalidade entre os players (concorrentes)
2. Barreira de Entrada e Barreira de Saida: possível novo entrante (novo player/concorrente)
3. Dos Fornecedores...
4. ... para os Consumidores
5. Força dos substitutos: os substitutos têm chance de destruir o setor/industria


## SWOT

1. SWOT é uma maneira de analisar o ambiente interno vs externo dos 3 C's

```
                AI
                |
                |
            S   |   W
            ++  |   --
                |
----------------+----------------
                |
            ++  |   --
            O   |   T
                |
                |
                AE
```

Bisu para fazer o SWOT:

1. 3 colunas: cia, cliente, concorrencia
2. Cia é o ambiente interno
3. Clientes + Concorrencia são o ambiente externo
4. Lista dados dos ambientes sem criticar se é força ou fraqueza, oportunidade ou risco
5. Classifica os pontos
6. Transporta para a matriz SWOT

O que for respondido com "como" é parte da estratégia: "como eu chego lá (objetivo)?".

#### Como usar o SWOT para decidir a abordagem estratégica?

1. Muita força + Muita oportunidade 	 = Agressivo
2. Muita fraqueza + Muitas ameaças 	 = Defensivo
3. Muita força + Muita ameaça 		 = Diversificação
4. Muitas fraquezas + Muita oportunidade = Recuperação (investir em transformar fraquezas em forças para aproveitar as oportunidades)

**O SWOT bate uma foto e sugere uma ação.**

## Extras

1. Conceitos de marketing (análise do mercado e aprender) vs Lean Startup
	1. Tamanho do ciclo de aprendizado
	2. Mais dinamismo implica menores ciclos de aprendizado/analise


**Meu SWOT Profissional**

## Edulify.com

1. Qual é o mercado substituto para e-learning systems?
2. SWOT para o Edulify.com como empresa e como produto

